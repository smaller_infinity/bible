package bible.data

import bible.data.types._
import bible.types._

import com.typesafe.scalalogging.Logger
import scalaj.http._
import io.circe._
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.generic.semiauto.deriveDecoder
import io.lemonlabs.uri.{ Url, UrlPath }
import scala.io.Source
import cats.implicits._

case class API(apiKey: String) {
  private val log = Logger("bible-api")
  private val baseURL = Url("https://api.scripture.api.bible")

  def getBibles(language: String): Either[APIFailure, List[Bible]] = {
    val url = baseURL.withPath(UrlPath(List("v1", "bibles"))).toString()
    log.info(s"Getting all bibles in ${language} from ${url}")
    val response = Http(url).method("GET").param("language", language)
      .headers(("Content-Type", "application/json"), ("api-key", apiKey))
      .execute({inputStream => Source.fromInputStream(inputStream).mkString})
    implicit val bibleDecoder = deriveDecoder[Bible]
    implicit val dataDecoder = deriveDecoder[BiblesApiResponse]
    parser.decode[BiblesApiResponse](response.body) match {
      case Right(response) => Right(response.data)
      case Left(error) => Left(JsonError(error))
    }
  }

  def getChapterCodes(bibleId: String): Either[APIFailure, List[ChapterCode]] = {
    val url = baseURL.withPath(UrlPath(List("v1", "bibles", s"$bibleId", "books")))
      .toString()
    log.info(s"Getting all chapters for ${bibleId} from ${url}")
    val response = Http(url).method("GET").param("include-chapters", "true")
      .headers(("Content-Type", "application/json"), ("api-key", apiKey))
      .execute{ inputStream => Source.fromInputStream(inputStream).mkString }
    implicit val chapterDecoder = deriveDecoder[ChapterCode]
    implicit val bookDecoder = deriveDecoder[ChapterCodesApiResponse]
    implicit val dataDecoder = deriveDecoder[BooksApiResponse]
    parser.decode[BooksApiResponse](response.body) match {
      case Right(response) => Right(response.data.flatMap(x => x.chapters))
      case Left(error) => Left(JsonError(error))
    }
  }

  def getChapter(bibleIds: List[String], chapterCode: String):
      Either[APIFailure, List[Chapter]] = {
    val url = baseURL.withPath(
      UrlPath(List("v1", "bibles", s"${bibleIds.head}",
                   "chapters", s"$chapterCode"))).toString()
    log.info(s"Getting chapter $chapterCode for bible $bibleIds")
    var working = true
    try {
      val response =
        Http(url).method("GET").params(("content-type", "text"), ("include-titles", "false"),
                                       ("parallels", bibleIds.tail.mkString(",")))
          .headers(("Content-Type", "application/json"), ("api-key", apiKey))
          .timeout(1000, 30000)
          .execute{ inputStream => Source.fromInputStream(inputStream).mkString }
      implicit val chapterDecoder = deriveDecoder[Chapter]
      implicit val firstChapterDecoder = deriveDecoder[HeadChapter]
      implicit val dataDecoder = deriveDecoder[ChapterApiResponse]
      parser.decode[ChapterApiResponse](response.body) match {
        case Right(response) =>
          val first = Chapter(response.data.reference,
                              response.data.content,
                              response.data.bibleId)
          Right((first :: response.data.parallels).map(c => c.dropNewLines()))
        case Left(error) => Left(JsonError(error))
      }
    }
    catch {
      case e: com.twitter.util.TimeoutException =>
        Left(TimeoutError())
      case e: java.net.SocketTimeoutException =>
        Left(TimeoutError())
    }
  }
}
