package bible.data.types

import io.circe.Error
import scalaj.http.HttpRequest
import bible.types._

sealed trait APIFailure
case class JsonError(failure: Error) extends APIFailure
case class ResponseFailure(statusCode: Int, error: String, message: String) extends APIFailure
case class TimeoutError() extends APIFailure

case class BiblesApiResponse(data: List[Bible])

case class ChapterCodesApiResponse(chapters: List[ChapterCode])
case class BooksApiResponse(data: List[ChapterCodesApiResponse])

case class HeadChapter(reference: String, content: String, bibleId: String,
                       parallels: List[Chapter])
case class ChapterApiResponse(data: HeadChapter)
