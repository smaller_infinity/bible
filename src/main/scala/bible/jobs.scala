package bible.jobs

import bible.Database

import java.util.UUID.randomUUID
import scala.concurrent.stm.{TMap, atomic}

sealed trait JobStatus
case class InProgress() extends JobStatus {
  override def toString(): String = {
    "IN-PROGRESS"
  }
}
case class Succeeded() extends JobStatus {
  override def toString(): String = {
    "SUCCEEDED"
  }
}
case class Failed() extends JobStatus {
  override def toString(): String = {
    "FAILED"
  }
}

case class FetchJob(
  description: String, totalTasks: Int,
  uuid: String = randomUUID().toString(),
  tasksCompleted: Int = 0, status: JobStatus = InProgress()) {
  def completeTask: FetchJob = {
    val newCompleted = this.tasksCompleted + 1
    FetchJob(this.description, this.totalTasks, this.uuid, newCompleted,
             if (newCompleted == this.totalTasks) Succeeded() else this.status)
  }

  def failJob: FetchJob = {
    FetchJob(this.description, this.totalTasks, this.uuid, this.tasksCompleted, Failed())
  }
}

case class JobLedger(database: Database) {
  val sessionLedger: TMap[String, FetchJob] = TMap()

  def start = {
    database.failOldJobs()
  }

  def addJob(description: String, totalTasks: Int): String = {
    val job = FetchJob(description, totalTasks)
    atomic { implicit tnx =>
      sessionLedger += (job.uuid -> job)
      database.insertJob(job)
    }
    job.uuid
  }

  def completeTask(jobUUID: String): JobStatus = {
    atomic { implicit tnx =>
      val job = sessionLedger(jobUUID)
      val newJob = job.completeTask
      sessionLedger += (jobUUID -> newJob)
      if (job.status != newJob.status) {
        database.updateJobStatus(jobUUID, newJob.status)
      }
      newJob.status
    }
  }

  def failJob(jobUUID: String) = {
    atomic { implicit tnx =>
      val job = sessionLedger(jobUUID)
      val newJob = job.failJob
      sessionLedger += (jobUUID -> newJob)
      database.updateJobStatus(jobUUID, newJob.status)
    }
  }

  def getJob(uuid: String): Option[FetchJob] = {
    atomic { implicit tnx =>
      sessionLedger get uuid
    }
  }
}
