package bible.types

case class Bible(id: String, name: String, abbreviation: String, description: String)
case class ChapterCode(id: String)
case class Chapter(reference: String, content: String, bibleId: String) {
  def dropNewLines(): Chapter = {
    Chapter(this.reference, this.content.filter(p => p != '\n'), this.bibleId)
  }
}
