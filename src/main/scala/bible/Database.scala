package bible

import bible.types._
import bible.jobs._

import doobie._
import doobie.implicits._
import doobie.util.ExecutionContexts
import cats._
import cats.data._
import cats.effect.IO
import cats.effect.IO._
import cats.implicits._
import fs2.Stream

case class Database() {
  private val queries = "?zeroDateTimeBehavior=convertToNull"
  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)
  private val transactor = Transactor.fromDriverManager[IO](
    "com.mysql.jdbc.Driver",
    "jdbc:mysql://" ++ System.getenv("DATABASE_HOST") ++ "/" ++
      System.getenv("DATABASE_NAME") ++ queries,
    System.getenv("DATABASE_USER"),
    System.getenv("DATABASE_PASSWORD"),
    ExecutionContexts.synchronous)

  def insertLanguageAndGetId(code: String): Int = {
    val sql = for {
      _ <- sql"INSERT IGNORE INTO languages (language_code) VALUES ($code)".update.run
      id <- sql"SELECT language_id FROM languages WHERE language_code = $code".query[Int].unique
    } yield id
    sql.transact(transactor).unsafeRunSync()
  }

  def getBibles(): List[(Int,Bible)] = {
    sql"SELECT api_id, bible_name, abbrevation, description, bible_id FROM bibles;"
      .query[(String, String, String, String, Int)]
      .to[List].transact(transactor)
      .flatMap(records =>
        IO(records.map(record =>
             (record._5, Bible(record._1, record._2, record._3, record._4)))))
      .unsafeRunSync()
  }

  def getBibles(language: String): List[(Int, Bible)] = {
    sql"""
SELECT api_id, bible_name, abbrevation, description, bible_id FROM bibles
JOIN languages ON languages.language_id = bibles.language_id
WHERE language_code = $language
""".query[(String, String, String, String, Int)]
      .to[List].transact(transactor)
      .flatMap(records =>
        IO(records.map(record =>
             (record._5, Bible(record._1, record._2, record._3, record._4)))))
      .unsafeRunSync()
  }

  def insertManyBibles(bibles: List[Bible], languageId: Int): Int = {
    val sql = """
INSERT IGNORE INTO bibles (api_id, bible_name, abbrevation, description, language_id)
VALUES (?, ?, ?, ?, ?)
    """
    Update[(Bible, Int)](sql).updateMany(bibles.map(b => (b, languageId)))
      .transact(transactor).unsafeRunSync()
  }

  def getChapterCodes(): List[ChapterCode] = {
    sql"SELECT chapter_code FROM chapter_codes;".query[String].to[List]
      .transact(transactor).unsafeRunSync().map(x => ChapterCode(x))
  }

  def insertChapterCodes(chapters: List[ChapterCode]): Int = {
    val sql = "INSERT IGNORE INTO chapter_codes (chapter_code) VALUES (?)"
    Update[ChapterCode](sql).updateMany(chapters).transact(transactor).unsafeRunSync()
  }

  def getChapters(code: String): List[Chapter] = {
    sql"""
SELECT reference, content, bibles.api_id FROM chapters
JOIN bibles ON bibles.bible_id = chapters.bible_id
JOIN chapter_codes ON chapter_codes.chapter_code_id = chapters.chapter_code_id
WHERE chapter_code = $code
""".query[Chapter].to[List].transact(transactor).unsafeRunSync()
  }

  def getChapter(code: String, bible_id: String): Chapter = {
    sql"""
SELECT reference, content, bibles.api_id FROM chapters
JOIN bibles ON bibles.bible_id = chapters.bible_id
JOIN chapter_codes ON chapter_codes.chapter_code_id = chapters.chapter_code_id
WHERE chapter_code = $code AND bible.bible_id = $bible_id
""".query[Chapter].unique.transact(transactor).unsafeRunSync()
  }

  // This should really be batch, but oh well
  def insertChapter(chapter: Chapter, chapterCode: String): Boolean = {
    sql"""
INSERT IGNORE INTO chapters (reference, content, chapter_code_id, bible_id)
SELECT ${chapter.reference}, ${chapter.content}, chapter_codes.chapter_code_id, bibles.bible_id FROM bibles
CROSS JOIN chapter_codes
WHERE chapter_codes.chapter_code = $chapterCode AND bibles.api_id = ${chapter.bibleId};
""".update.run.transact(transactor).unsafeRunSync() == 1
  }

  def insertJob(job: FetchJob) = {
    sql"""
INSERT INTO jobs (uuid, description, status)
VALUES (${job.uuid}, ${job.description}, ${job.status.toString()});
""".update.run.transact(transactor).unsafeRunSync()
  }

  def failOldJobs() = {
    sql"""
UPDATE jobs SET status = ${Failed().toString()}
WHERE status = ${InProgress().toString()};
""".update.run.transact(transactor).unsafeRunSync()
  }

  def updateJobStatus(uuid: String, status: JobStatus) = {
    sql"""
UPDATE jobs SET status = ${status.toString()}
WHERE uuid = $uuid;
""".update.run.transact(transactor).unsafeRunSync()
  }

  def getJobs(): List[(String, String, String, String, Option[String])] = {
    sql"SELECT uuid, description, status, created, completed FROM jobs;"
      .query[(String, String, String, String, Option[String])].to[List]
      .transact(transactor).unsafeRunSync()
  }

  def getJob(uuid: String): (String, String, String, String, Option[String]) = {
    sql"SELECT uuid, description, status, created, completed FROM jobs WHERE uuid = $uuid;"
      .query[(String, String, String, String, Option[String])].unique
      .transact(transactor).unsafeRunSync()
  }
}
