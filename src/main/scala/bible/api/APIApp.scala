package bible.api

import bible.api.services.CollectorService
import bible.api.types._
import bible.Database
import bible.jobs.JobLedger
import com.twitter.app.Flag
import com.twitter.finagle.Http
import com.twitter.finagle.http.Status
import com.twitter.server.TwitterServer
import com.twitter.util.Await
import io.circe.generic.auto._
import io.finch.{Endpoint, _}
import io.finch.circe._
import cats.effect.IO
import com.typesafe.scalalogging.Logger

object APIApp extends TwitterServer with Endpoint.Module[IO] {
  val port: Flag[Int] = flag("port", 8086, "TCP port for HTTP server")
  val log = Logger("API")
  val database = Database()
  val ledger = JobLedger(database)
  ledger.start
  val collectorService = CollectorService(database, ledger)
  val jobService = JobService(database, ledger)

  def fetchBibles: Endpoint[IO, FetchBiblesSuccess] =
    post("bibles" :: "fetch" :: path[String] :: header("api-key")) {
      (request: String, apiKey: String) =>
      collectorService.fetchBibles(request, apiKey).map(Ok)
    }

  def getBibles: Endpoint[IO, GetBiblesResponse] =
    get("bibles") {
      collectorService.getBibles().map(Ok)
    }

  def getBiblesByLanguage: Endpoint[IO, GetBiblesResponse] =
    get("bibles" :: path[String]) {
      language: String => collectorService.getBibles(language).map(Ok)
    }

  def fetchChapterCodes: Endpoint[IO, AsyncResponse] =
    post("chapter-codes" :: "fetch" :: path[String] :: header("api-key")) {
      (request: String, apiKey: String) =>
      collectorService.fetchChapters(request, apiKey)
        .map(x => Ok(x).withStatus(Status(201)))
    }

  def getChapterCodes: Endpoint[IO, GetChapterCodesResponse] =
    get("chapter-codes") {
      collectorService.getChapterCodes().map(Ok)
    }

  def getJobs: Endpoint[IO, GetJobsResponse] =
    get("jobs") {
      jobService.getJobs().map(Ok)
    }

  def getJob: Endpoint[IO, GetJobResponse] =
    get("jobs" :: path[String]) {
      (uuid: String) => jobService.getJob(uuid).map(Ok)
    }

  def fetchChapters: Endpoint[IO, AsyncResponse] =
    post("chapters" :: "fetch" :: path[String] :: header("api-key")) {
      (language: String, apiKey: String) =>
      collectorService.fetchText(language, apiKey)
        .map(x => Ok(x).withStatus(Status(201)))
    }

  def getChapters: Endpoint[IO, GetChaptersResponse] =
    get("chapters" :: path[String]) {
      (code: String) =>
      collectorService.getTexts(code).map(Ok)
    }

  def getChapter: Endpoint[IO, GetChapterResponse] =
    get("chapters" :: path[String] :: path[String]) {
      (code: String, bible: String) =>
      collectorService.getText(code, bible).map(Ok)
    }

  val api =
    (fetchBibles :+: getBibles :+: getBiblesByLanguage :+: fetchChapterCodes :+:
       getChapterCodes :+: fetchChapters :+: getJobs :+: getJob :+: getChapters :+:
       getChapter).handle {
      case e: Exception =>
        log.error(e.getMessage)
        InternalServerError(e)
    }

  def main(): Unit = {
    log.info(s"Starting API on port ${port()}")
    val server =
      Http.server.withStatsReceiver(statsReceiver)
        .serve(s":${port()}", api.toServiceAs[Application.Json])
    closeOnExit(server)

    Await.ready(adminHttpServer)
  }
}
