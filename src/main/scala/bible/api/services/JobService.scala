package bible.api

import bible.Database
import bible.jobs._
import bible.api.types._

import com.twitter.util.Future

case class JobService(database: Database, ledger: JobLedger) {
  private def buildGetJobResponse(dbJob: (String, String, String, String, Option[String])):
      GetJobResponse = {
    ledger.getJob(dbJob._1) match {
      case None =>
        GetJobResponse(
          dbJob._1, dbJob._3, dbJob._2, dbJob._4, dbJob._5, false, None, None)
      case Some(job) =>
        GetJobResponse(
          dbJob._1, dbJob._3, dbJob._2, dbJob._4, dbJob._5, true,
          Some(job.tasksCompleted), Some(job.totalTasks))
    }
  }

  def getJob(uuid: String): Future[GetJobResponse] = {
    Future.value{
      buildGetJobResponse(database.getJob(uuid))
    }
  }

  def getJobs(): Future[GetJobsResponse] = {
    Future.value {
      GetJobsResponse(database.getJobs().map(x => buildGetJobResponse(x)))
    }
  }
}
