package bible.api.services

import bible.Database
import bible.jobs._
import bible.api.types._
import bible.data.API
import bible.data.types.JsonError
import com.twitter.util.Future
import com.typesafe.scalalogging.Logger

case class CollectorService(database: Database, ledger: JobLedger) {
  private val logger = Logger("collector-service")

  def fetchBibles(language: String, apiKey: String): Future[FetchBiblesSuccess] = {
    Future.value{
      val languageId = database.insertLanguageAndGetId(language)
      val bibles = API(apiKey).getBibles(language)
      FetchBiblesSuccess(database.insertManyBibles(bibles.right.get, languageId))
    }
  }

  def getBibles(): Future[GetBiblesResponse] = {
    Future.value {
      GetBiblesResponse(database.getBibles().map(x => x._2))
    }
  }

  def getBibles(language: String) = {
    Future.value {
      GetBiblesResponse(database.getBibles(language).map(x => x._2))
    }
  }

  def fetchChapters(language: String, apiKey: String):
      Future[AsyncResponse] = {
    Future.value {
      val bibleIds = database.getBibles(language).map(x => x._2.id)
      val bibles = bibleIds.length
      val uuid = ledger.addJob(
        s"Getting chapter codes for $bibles $language bibles", bibles + 1)
      val thread = new Thread {
        override def run {
          try {
            val codes = bibleIds.flatMap{id =>
              val tmp = API(apiKey).getChapterCodes(id).right.get
              ledger.completeTask(uuid)
              tmp
            }
            database.insertChapterCodes(codes)
            ledger.completeTask(uuid)
          }
          catch {
            case e: Exception =>
              logger.error(e.getMessage())
              ledger.failJob(uuid)
          }
        }
      }
      thread.start()
      AsyncResponse(uuid)
    }
  }

  def getChapterCodes(): Future[GetChapterCodesResponse] = {
    Future.value {
      GetChapterCodesResponse(database.getChapterCodes())
    }
  }

  def getTexts(chapterCode: String): Future[GetChaptersResponse] = {
    Future.value(GetChaptersResponse(database.getChapters(chapterCode)))
  }

  def getText(code: String, bible: String): Future[GetChapterResponse] = {
    Future.value(GetChapterResponse(database.getChapter(code, bible)))
  }

  def fetchText(language: String, apiKey: String): Future[AsyncResponse] = {
    Future.value {
      val bibles = database.getBibles(language).map(bible => bible._2.id)
      val numBibles = bibles.length
      val codes = database.getChapterCodes().map(code => code.id)
      val downloads = codes.length
      val uuid = ledger.addJob(
        s"Getting $downloads chapters for $numBibles $language bibles",
        downloads * 2)
      val api = API(apiKey)
      val thread = new Thread {
        override def run {
          try {
            codes.foreach { code =>
              var working = true
              while (working) {
                api.getChapter(bibles, code) match {
                  case Right(chapters) =>
                    ledger.completeTask(uuid)
                    chapters.foreach { chapter =>
                      database.insertChapter(chapter, code)
                    }
                    ledger.completeTask(uuid)
                    working = false
                  case Left(JsonError(_)) =>
                    working = false
                    ledger.completeTask(uuid)
                    ledger.completeTask(uuid)
                  case Left(e) =>
                    logger.error(e.toString())
                    Thread.sleep(3000)
                    logger.warn("Request failed, retrying")
                }
              }
            }
          }
          catch {
            case e: Exception =>
              logger.error(e.getMessage())
              ledger.failJob(uuid)
          }
        }
      }
      thread.start()
      AsyncResponse(uuid)
    }
  }
}
