package bible.api.types

import bible.types._

case class FetchBiblesSuccess(bibles_updated: Int)
case class GetBiblesResponse(bibles: List[Bible])
case class AsyncResponse(job_uuid: String)
case class GetChapterCodesResponse(chapters: List[ChapterCode])
case class GetChaptersResponse(chapters: List[Chapter])
case class GetChapterResponse(chapter: Chapter)

case class GetJobResponse(
  uuid: String, status: String, description: String, created: String,
  completed: Option[String], in_session: Boolean, tasks_completed: Option[Int],
  total_tasks: Option[Int])
case class GetJobsResponse(jobs: List[GetJobResponse])
