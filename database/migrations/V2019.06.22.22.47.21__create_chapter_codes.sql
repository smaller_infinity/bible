CREATE TABLE chapter_codes (
       chapter_code_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
       chapter_code VARCHAR(50) NOT NULL,

       CONSTRAINT chapters_pk
       PRIMARY KEY (chapter_code_id),

       CONSTRAINT chapters_ak1
       UNIQUE (chapter_code)
);
