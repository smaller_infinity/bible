CREATE TABLE jobs (
       job_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
       uuid VARCHAR(50) NOT NULL,
       description VARCHAR(50) NOT NULL,
       status VARCHAR(50) NOT NULL,
       created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
       completed TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

       CONSTRAINT job_pk
       PRIMARY KEY (job_id),

       INDEX job_ix1 (uuid)
);
