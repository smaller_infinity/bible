CREATE TABLE bibles (
       bible_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
       api_id VARCHAR(50) NOT NULL,
       bible_name VARCHAR(50) NOT NULL,
       abbrevation VARCHAR(255) NOT NULL,
       description VARCHAR(255) NOT NULL,
       language_id INT UNSIGNED NOT NULL,

       CONSTRAINT bible_pk
       PRIMARY KEY (bible_id),

       CONSTRAINT bible_fk1
       FOREIGN KEY (language_id) REFERENCES languages (language_id),

       CONSTRAINT bible_ak1
       UNIQUE (api_id)
);
