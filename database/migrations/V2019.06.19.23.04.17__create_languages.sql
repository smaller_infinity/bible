CREATE TABLE languages (
       language_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
       language_code CHAR(3) NOT NULL,

       CONSTRAINT languages_pk
       PRIMARY KEY(language_id),

       CONSTRAINT languages_ak1
       UNIQUE(language_code)
);
