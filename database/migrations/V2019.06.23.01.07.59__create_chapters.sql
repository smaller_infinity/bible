CREATE TABLE chapters (
       chapter_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
       reference VARCHAR(50) NOT NULL,
       content TEXT NOT NULL,
       chapter_code_id INT UNSIGNED NOT NULL,
       bible_id INT UNSIGNED NOT NULL,

       CONSTRAINT chapters_pk
       PRIMARY KEY (chapter_id),

       CONSTRAINT chapters_fk1
       FOREIGN KEY (chapter_code_id) REFERENCES chapter_codes (chapter_code_id),

       CONSTRAINT chapters_fk2
       FOREIGN KEY (bible_id) REFERENCES bibles (bible_id),

       CONSTRAINT chapters_ak1
       UNIQUE (bible_id, chapter_code_id)
);
