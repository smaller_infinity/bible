#!/usr/bin/env bash

# Creates a file in the migration database that that uses date
# and

# For Usage: bin/create_migration.sh --help

# Note - this script assumes that hailstorm, aperture_api, and sfauth auth are installed in
# the same directory as the report.  It also assumes that each project's migrations are
# stored in a directory called database/migrations

print_help() {
    echo
    echo "Creates a file in the project_name/database/migration directory"
    echo "in the form 'VYYYY.MM.DD.HH.MM.SS__<project_name>_<migration_type>_<migration_name>.sql'"
    echo
    echo "Usage"
    echo "([-c | --create] | [-d | --drop] | [-a | --alter] | [ -r | --rename] [-g | --grant] | [-i | --insert]) <migration name>"
    echo
    echo "Example: ./create_migration.sh --create add_users_table"
    echo
    echo "Options:"
    echo "--help prints this help"

}

if [[ ! $# -eq 2 ]]; then
    print_help
    exit
fi

for i in "$@"
do
    case $i in
	--help)
	    print_help
	    exit
	    ;;
	-c|--create)
	    migration_type="create"
	    shift
	    ;;
	-d|--drop)
	    migration_type="drop"
	    shift
	    ;;
	-a|--alter)
	    migration_type="alter"
	    shift
	    ;;
	-r|--rename)
	    migration_type="rename"
	    shift
	    ;;
	-g|--grant)
	    migration_type="grant"
	    shift
	    ;;
	-i|--insert)
	    migration_type="insert"
	    shift
	    ;;
    esac
done

migration_directory="database/migrations/"


if [[ ! -d "$migration_directory" ]]
then
    echo "The directory ${migration_directory} does not exist"
    exit
else
    current_time="$(date +%Y.%m.%d.%H.%M.%S)"
    file_name="V${current_time}__${migration_type}_$1.sql"

    cat << 'EOF' > "${migration_directory}${file_name}"
-- Tips (feel free to delete)
--
-- Migrations SHOULD serve a single purpose.
--
-- Migration SHOULD NOT contain values, unless those values are required for the app to run.
--
EOF


    echo
    echo "#########################"
    echo "#########################"
    echo "#######'         '#######"
    echo "####'               '####"
    echo "###                   ###"
    echo "##        |\           ##"
    echo "#        |V \_          #"
    echo "#        |  ' \         #"
    echo "#        )   ,_\        #"
    echo "#       /    |          #"
    echo "##     /      \        ##"
    echo "###    |       \      ###"
    echo "####.   \       \   .####"
    echo "######-._|       \.######"
    echo "#########| |#     |######"
    echo "###jgs###/ |#'.   /######"
    echo "#########################"
    echo "#########################"
    echo

    echo "Created migration ${file_name} in directory: ${migration_directory}"
fi
