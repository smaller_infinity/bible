#!/usr/bin/env bash

if [ ! -f "env.sh" ]; then
    echo "Please create '../env.sh' from env.sh.template to be sourced"
    exit
else
    source "env.sh"
fi

user="${DATABASE_USER:?Need to set environmental variable DATABASE_USER}"
pass="${DATABASE_PASSWORD:?Need to set environmental variable DATABASE_PASSWORD}"
host="${DATABASE_HOST:?Need to set environmental variable DATABASE_HOST}"
db="${DATABASE_NAME:?Need to set environmental variable DATABASE_NAME}"

sql=$(printf "CREATE USER IF NOT EXISTS '%s' IDENTIFIED BY '%s';\\n " $user $pass)
sql+="CREATE DATABASE IF NOT EXISTS ${db} DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;\\n ";
sql+=$(printf "GRANT ALL PRIVILEGES ON %s.* TO '%s'@'%s' IDENTIFIED BY '%s';\\n " $db $user $host $pass)
sql+=$(printf "GRANT ALL PRIVILEGES ON \`%%\` . * TO '%s'@'%s' IDENTIFIED BY '%s';\\n " $user $host $pass)
sql+="FLUSH PRIVILEGES;\\n "

echo "Input Password for The Mysql root user: "

mysql -h ${host} -u root -p --execute="${sql}"

if [ $? -eq 0 ]; then
    echo "Created user ${user} and database ${host}.  Granted all privileges to ${user}"
else
    echo "Failed to execute SQL: ${sql}"
fi
