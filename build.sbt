name := "bible"
version := "0.1"

lazy val circeVersion = "0.10.0"
lazy val doobieVersion = "0.7.0"
lazy val finchVersion = "0.29.0"
scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalaj" %% "scalaj-http" % "2.4.1",
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.lemonlabs" %% "scala-uri" % "1.4.5",
  "org.tpolecat" %% "doobie-core"     % doobieVersion,
  "org.tpolecat" %% "doobie-postgres" % doobieVersion,
  "org.tpolecat" %% "doobie-specs2"   % doobieVersion,
  "mysql" % "mysql-connector-java" % "5.1.24",
  "com.github.finagle" %% "finchx-core" % finchVersion,
  "com.github.finagle" %% "finchx-circe" % finchVersion,
  "com.twitter" %% "twitter-server" % "19.5.1",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.github.nscala-time" %% "nscala-time" % "2.22.0" % Test,
  "org.scala-stm" %% "scala-stm" % "0.8"
)
